# Render-To-Print-Blender-Addon

Render to print for blender 2.9+

Script used to set the size of the render for a print.
You can set the size in centimeters, change the DPI and, optionally, set the pixel size to obtain the actual size.
You can choose the size of the render from various print standard size.

![Render to print](https://www.marcocrippa.it/img/download/print_to_render.png)

Authors: Marco Crippa, Dealga McArdle, J.R.B.-Wein, Zheng Bo

